<div class="footer">
    <div class="copy-rights">
        <p>&copy; <?php echo date("Y") ?> - Cutting Edge . All Rights Reserved Design by <a href="http://www.vteccreations.com/">Vertec IT Solutions</a></p>
    </div>
    <div class="clearfix"></div>
</div>