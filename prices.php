<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>

<!-- Mirrored from p.w3layouts.com/demos/spring_spa/web/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 May 2016 11:21:19 GMT -->
<head>
    <title> Cutting Edge | Prices </title>
    <!--fonts-->
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700,300italic' rel='stylesheet'
          type='text/css'>
    <!--//fonts-->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords"
          content="Spring Spa Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design"/>
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);
        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <!-- //for-mobile-apps -->
    <script src="js/modernizr.custom.js"></script>
    <script src="js/jquery.min.js"></script>

    <link rel="stylesheet" type="text/css" href="background/css/style3.css" />
    <script type="text/javascript" src="background/js/modernizr.custom.86080.js"></script>

</head>
<body id="page">
<ul class="cb-slideshow">
    <li><span>Image 01</span><div><h3>Hair Iron</h3></div></li>
    <li><span>Image 02</span><div><h3>Hair cut</h3></div></li>
    <li><span>Image 03</span><div><h3>Baby Cut</h3></div></li>
    <li><span>Image 04</span><div><h3>Hair Iron</h3></div></li>
    <li><span>Image 05</span><div><h3>Oil Massage</h3></div></li>
    <li><span>Image 06</span><div><h3>Hair Colour</h3></div></li>
</ul>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '../../../../www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-30027142-1', 'w3layouts.com');
    ga('send', 'pageview');
</script>
<script async type='text/javascript'
        src='../../../../cdn.fancybar.net/ac/fancybar6a2f.js?zoneid=1502&amp;serve=C6ADVKE&amp;placement=w3layouts'
        id='_fancybar_js'></script>


<?php include('header.php') ?>

<div class="about" style="margin-bottom: 80px">
    <style>
        .panel-default {
            margin: 5px;
            font-weight: bold;
        }

        .panel-default > .panel-heading {
            background-color: #7CB8C1;
        }

        tbody > tr > td, .table > tfoot > tr > td {
            background-color: lavender;
        }
    </style>

    <div class="col-md-6">
        <h2 style="text-align: center">Ladies</h2>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Hair Cut</h3></div>
            <!-- List group -->
            <table class="table table-inverse">
                <tbody>
                <tr>
                    <td>Straight Cut</td>
                    <td>200</td>
                </tr>
                <tr>
                    <td>u/v Cut</td>
                    <td>350</td>
                </tr>
                <tr>
                    <td>Baby Cut</td>
                    <td>300</td>
                </tr>
                <tr>
                    <td>Short Hair Cut</td>
                    <td>400</td>
                </tr>
                <tr>
                    <td>Long layer</td>
                    <td>600 - upwards</td>
                </tr>
                <tr>
                    <td>Short layer</td>
                    <td>400 - upwards</td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Threading</h3></div>
            <!-- List group -->
            <table class="table table-inverse">
                <tbody>
                <tr>
                    <td>Eye brow</td>
                    <td>500</td>
                </tr>
                <tr>
                    <td>upper lip</td>
                    <td>50</td>
                </tr>
                <tr>
                    <td>Chin</td>
                    <td>50</td>
                </tr>
                <tr>
                    <td>Forhead</td>
                    <td>50</td>
                </tr>
                <tr>
                    <td>Sidebump</td>
                    <td>50</td>
                </tr>
                <tr>
                    <td>Full face</td>
                    <td>600</td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Clean Up</h3></div>
            <!-- List group -->
            <table class="table table-inverse">
                <tbody>
                <tr>
                    <td>Jovees</td>
                    <td>1000</td>
                </tr>
                <tr>
                    <td>Himalaya</td>
                    <td>850</td>
                </tr>
                <tr>
                    <td>Normal Facial</td>
                    <td>Jovees 1800 </br>Himalaya 1600</td>
                </tr>
                <tr>
                    <td>Gold facial</td>
                    <td>2700</td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Hair Colour</h3></div>
            <!-- List group -->
            <table class="table table-inverse">
                <tbody>
                <tr>
                    <td>Fashion Colour</td>
                    <td>1000 - upwards</td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Wax</h3></div>
            <!-- List group -->
            <table class="table table-inverse">
                <tbody>
                <tr>
                    <td>under arm</td>
                    <td>350</td>
                </tr>
                <tr>
                    <td>Hand Wax</td>
                    <td>950</td>
                </tr>
                <tr>
                    <td>Full leg Wax</td>
                    <td>1500</td>
                </tr>
                <tr>
                    <td>Half leg Wax</td>
                    <td>800</td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Treatment</h3></div>
            <!-- List group -->
            <table class="table table-inverse">
                <tbody>
                <tr>
                    <td>Oil Treatment</td>
                    <td>800 - upwards</td>
                </tr>
                <tr>
                    <td>Conditioner Treatment</td>
                    <td>1200</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Hair Iron - 900 upwards</h3></div>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Hair Setting - 800 upwards</h3></div>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Cutting Setting - 900 upwards</h3></div>
        </div>
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Makeup - 800</h3></div>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Hair Style - 800</h3></div>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Saree Dressing - 500</h3></div>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Full Dressing - 1500</h3></div>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Maniqure - 1600</h3></div>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Pediqure - 2600</h3></div>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Bonding - 5000 upwards</h3></div>
        </div>
    </div>

    <div class="col-md-6">
        <h2 style="text-align: center">Gents</h2>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Hair cut 400 - upwards</h3></div>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Sewing 200 - upwards</h3></div>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Baby Cut - 250</h3></div>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Eye brow - 250</h3></div>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Normal facial</h3></div>
            <!-- List group -->
            <table class="table table-inverse">
                <tbody>
                <tr>
                    <td>Jovees</td>
                    <td>1800</td>
                </tr>
                <tr>
                    <td>Himalaya</td>
                    <td>1600</td>
                </tr>
                <tr>
                    <td>Gold facial</td>
                    <td>2700</td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Hair Colour</h3></div>
            <!-- List group -->
            <table class="table table-inverse">
                <tbody>
                <tr>
                    <td>Fashion Colour</td>
                    <td>100 - upwards</td>
                </tr>
                <tr>
                    <td>Black Colour</td>
                    <td>100 - upwards</td>
                </tr>
                <tr>
                    <td>Henna Colour</td>
                    <td>500 - upwards</td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Massage</h3></div>
            <!-- List group -->
            <table class="table table-inverse">
                <tbody>
                <tr>
                    <td>Tonic Massage</td>
                    <td>400</td>
                </tr>
                <tr>
                    <td>Bcheram Massage</td>
                    <td>300</td>
                </tr>
                <tr>
                    <td>Oil Massage</td>
                    <td>600</td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Treatment   1000</h3></div>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Conditioner 1000</h3></div>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Hair wash - 300 upwards</h3></div>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Maniqure - 1600</h3></div>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Pediqure - 2600</h3></div>
        </div>

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Bonding - 5000 upwards</h3></div>
        </div>
    </div>


</div>


<script type="text/javascript">
    $(function () {

        var BlurBGImage = (function () {

            var $bxWrapper = $('#bx-wrapper'),
            // loading status to show while preloading images
                $bxLoading = $bxWrapper.find('div.bx-loading'),
            // container for the bg images and respective canvas
                $bxContainer = $bxWrapper.find('div.bx-container'),
            // the bg images we are gonna use
                $bxImgs = $bxContainer.children('img'),
            // total number of bg images
                bxImgsCount = $bxImgs.length,
            // the thumb elements
                $thumbs = $bxWrapper.find('div.bx-thumbs > a').hide(),
            // the title for the current image
                $title = $bxWrapper.find('h2:first'),
            // current image's index
                current = 0,
            // variation to show the image:
            // (1) - blurs the current one, fades out and shows the next image
            // (2) - blurs the current one, fades out, shows the next one (but initially blurred)
            // speed is the speed of the animation
            // blur Factor is the factor used in the StackBlur script
                animOptions = {speed: 700, variation: 2, blurFactor: 10},
            // control if currently animating
                isAnim = false,
            // check if canvas is supported
                supportCanvas = Modernizr.canvas,

            // init function
                init = function () {

                    // preload all images and respective canvas
                    var loaded = 0;

                    $bxImgs.each(function (i) {

                        var $bximg = $(this);

                        // save the position of the image in data-pos
                        $('<img data-pos="' + $bximg.index() + '"/>').load(function () {

                            var $img = $(this),
                            // size of image to be fullscreen and centered
                                dim = getImageDim($img.attr('src')),
                                pos = $img.data('pos');

                            // add the canvas to the DOM
                            $.when(createCanvas(pos, dim)).done(function () {

                                ++loaded;

                                // all images and canvas loaded
                                if (loaded === bxImgsCount) {

                                    // show thumbs
                                    $thumbs.fadeIn();

                                    // apply style for bg image and canvas
                                    centerImageCanvas();

                                    // hide loading status
                                    $bxLoading.hide();

                                    // initialize events
                                    initEvents();

                                }

                            });

                        }).attr('src', $bximg.attr('src'));

                    });

                },
            // creates the blurred canvas image
                createCanvas = function (pos, dim) {

                    return $.Deferred(function (dfd) {

                        // if canvas not supported return
                        if (!supportCanvas) {
                            dfd.resolve();
                            return false;
                        }

                        // create the canvas element:
                        // size and position will be the same like the fullscreen image
                        var $img = $bxImgs.eq(pos),
                            imgW = dim.width,
                            imgH = dim.height,
                            imgL = dim.left,
                            imgT = dim.top,

                            canvas = document.createElement('canvas');

                        canvas.className = 'bx-canvas';
                        canvas.width = imgW;
                        canvas.height = imgH;
                        canvas.style.width = imgW + 'px';
                        canvas.style.height = imgH + 'px';
                        canvas.style.left = imgL + 'px';
                        canvas.style.top = imgT + 'px';
                        canvas.style.visibility = 'hidden';
                        // save position of canvas to know which image this is linked to
                        canvas.setAttribute('data-pos', pos);
                        // append the canvas to the same container where the images are
                        $bxContainer.append(canvas);
                        // blur it using the StackBlur script
                        stackBlurImage($img.get(0), dim, canvas, animOptions.blurFactor, false, dfd.resolve);

                    }).promise();

                },
            // gets the image size and position in order to make it fullscreen and centered.
                getImageDim = function (img) {

                    var $img = new Image();

                    $img.src = img;

                    var $win = $(window),
                        w_w = $win.width(),
                        w_h = $win.height(),
                        r_w = w_h / w_w,
                        i_w = $img.width,
                        i_h = $img.height,
                        r_i = i_h / i_w,
                        new_w, new_h, new_left, new_top;

                    if (r_w > r_i) {

                        new_h = w_h;
                        new_w = w_h / r_i;

                    }
                    else {

                        new_h = w_w * r_i;
                        new_w = w_w;

                    }

                    return {
                        width: new_w,
                        height: new_h,
                        left: ( w_w - new_w ) / 2,
                        top: ( w_h - new_h ) / 2
                    };

                },
            // initialize the events
                initEvents = function () {

                    $(window).on('resize.BlurBGImage', function (event) {

                        // apply style for bg image and canvas
                        centerImageCanvas();
                        return false;

                    });

                    // clicking on a thumb shows the respective bg image
                    $thumbs.on('click.BlurBGImage', function (event) {

                        var $thumb = $(this),
                            pos = $thumb.index();

                        if (!isAnim && pos !== current) {

                            $thumbs.removeClass('bx-thumbs-current');
                            $thumb.addClass('bx-thumbs-current');
                            isAnim = true;
                            // show the bg image
                            showImage(pos);

                        }

                        return false;

                    });

                },
            // apply style for bg image and canvas
                centerImageCanvas = function () {

                    $bxImgs.each(function (i) {

                        var $bximg = $(this),
                            dim = getImageDim($bximg.attr('src')),
                            $currCanvas = $bxContainer.children('canvas[data-pos=' + $bximg.index() + ']'),
                            styleCSS = {
                                width: dim.width,
                                height: dim.height,
                                left: dim.left,
                                top: dim.top
                            };

                        $bximg.css(styleCSS);

                        if (supportCanvas)
                            $currCanvas.css(styleCSS);

                        if (i === current)
                            $bximg.show();

                    });

                },
            // shows the image at position "pos"
                showImage = function (pos) {

                    // current image
                    var $bxImage = $bxImgs.eq(current);
                    // current canvas
                    $bxCanvas = $bxContainer.children('canvas[data-pos=' + $bxImage.index() + ']'),
                        // next image to show
                        $bxNextImage = $bxImgs.eq(pos),
                        // next canvas to show
                        $bxNextCanvas = $bxContainer.children('canvas[data-pos=' + $bxNextImage.index() + ']');

                    // if canvas is supported
                    if (supportCanvas) {

                        $.when($title.fadeOut()).done(function () {

                            $title.text($bxNextImage.attr('title'));

                        });

                        $bxCanvas.css('z-index', 100).css('visibility', 'visible');

                        $.when($bxImage.fadeOut(animOptions.speed)).done(function () {

                            switch (animOptions.variation) {

                                case 1    :
                                    $title.fadeIn(animOptions.speed);
                                    $.when($bxNextImage.fadeIn(animOptions.speed)).done(function () {

                                        $bxCanvas.css('z-index', 1).css('visibility', 'hidden');
                                        current = pos;
                                        $bxNextCanvas.css('visibility', 'hidden');
                                        isAnim = false;

                                    });
                                    break;
                                case 2    :
                                    $bxNextCanvas.css('visibility', 'visible');

                                    $.when($bxCanvas.fadeOut(animOptions.speed * 1.5)).done(function () {

                                        $(this).css({
                                            'z-index': 1,
                                            'visibility': 'hidden'
                                        }).show();

                                        $title.fadeIn(animOptions.speed);

                                        $.when($bxNextImage.fadeIn(animOptions.speed)).done(function () {

                                            current = pos;
                                            $bxNextCanvas.css('visibility', 'hidden');
                                            isAnim = false;

                                        });

                                    });
                                    break;

                            }
                            ;

                        });

                    }
                    // if canvas is not shown just work with the bg images
                    else {

                        $title.hide().text($bxNextImage.attr('title')).fadeIn(animOptions.speed);
                        $.when($bxNextImage.css('z-index', 102).fadeIn(animOptions.speed)).done(function () {

                            current = pos;
                            $bxImage.hide();
                            $(this).css('z-index', 101);
                            isAnim = false;

                        });

                    }

                };

            return {
                init: init
            };

        })();

        // call the init function
        BlurBGImage.init();

    });
</script>

</div>
<div class="clearfix"></div>
<?php include('footer.php') ?>
</body>

<!-- Mirrored from p.w3layouts.com/demos/spring_spa/web/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 May 2016 11:21:19 GMT -->
</html>