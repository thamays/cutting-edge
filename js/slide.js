

var myImage = document.getElementById("mainImage");

var imageArray = ["images/a.jpg","images/b.jpg","images/c.jpg",
    "images/d.jpg","images/e.jpg"];
var imageIndex = 0;

function changeImage() {
    myImage.setAttribute("src",imageArray[imageIndex]);
    imageIndex++;
    if (imageIndex >= imageArray.length) {
        imageIndex = 0;
    }
}

// setInterval is also in milliseconds
var intervalHandle = setInterval(changeImage,500);

myImage.onclick =  function() {
    clearInterval(intervalHandle);
};